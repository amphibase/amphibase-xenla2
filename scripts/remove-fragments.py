#!/usr/bin/env python3
import gzip
import sys
import os

dirname_gff3_chr = os.path.join(
                    os.path.dirname(os.path.abspath(__file__)),
                    '..', 'gff3.by_chr')
dirname_tmp = os.path.join(
                    os.path.dirname(os.path.abspath(__file__)),
                    '..', 'tmp')
dirname_data = os.path.join(
                    os.path.dirname(os.path.abspath(__file__)),
                    '..', 'data')
filename_report = 'GCF_001663975.1_Xenopus_laevis_v2_assembly_report.txt.gz'

new_chr_list = dict()
f_report = gzip.open(os.path.join(dirname_data, filename_report), 'rt')
for line in f_report:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    sc_id = tokens[0]
    ucsc_id = tokens[-1]
    seq_len = int(tokens[-2])
    new_chr_list[sc_id] = {'len': seq_len, 'ucsc_id': ucsc_id}
f_report.close()


filename_log = 'fragmented.gff3'
f_log = open(filename_log, 'w')
f_log.write("#gff-version 3\n")

for tmp_filename in os.listdir(dirname_gff3_chr):

    old_filename = os.path.join(dirname_gff3_chr, tmp_filename)
    new_filename = os.path.join(dirname_tmp, tmp_filename)

    f_new = open(new_filename, 'w')
    f_new.write("#gff-version 3\n")

    sys.stderr.write('Process %s...\n' % old_filename)
    f = open(old_filename, 'r')
    for line in f:
        if line.startswith('#'):
            continue
        tokens = line.strip().split("\t")
        seq_id = tokens[0]
        if seq_id in new_chr_list:
            if seq_id == 'MT':
                new_seq_id = 'chrM'
            else:
                new_seq_id = new_chr_list[seq_id]['ucsc_id']

            if new_chr_list[seq_id]['len'] >= 5000:
                f_new.write('%s\t%s\n' % (new_seq_id, '\t'.join(tokens[1:])))
            else:
                f_log.write('%s\t%s\n' % (new_seq_id, '\t'.join(tokens[1:])))
        else:
            sys.stderr.write('Error: %s\n' % line.strip())
    f.close()
    f_new.close()

f_log.close()
